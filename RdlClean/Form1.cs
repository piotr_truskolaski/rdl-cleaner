﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RdlClean
{
    public partial class Form1 : Form
    {
        private const decimal Onemilimeter = 2.834645669291M;
        private const decimal OneCentimeter = 28.34645669291M;

        public Form1()
        {
            InitializeComponent();

            openFileDialog1.Filter = "Report file |*.rdl";
            openFileDialog1.Title = "Select a Report File";

        }
        private string rdlcontent = "";
        private void openRdlRaportFileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        public static string GetDecimalRound(decimal Argument)
        {
            return  Int64.Parse(decimal.Round(Argument, 0).ToString()).ToString();
            
        }
        

        public string FindPositionsToConvertAndFixThem(string reportBody)
        {
            Regex regx = new Regex(@"(?:\d*\.)?\d+cm|(?:\d*\.)?\d+mm|(?:\d*\.)?\d+pt", RegexOptions.Compiled);

            Regex regxMeasure = new Regex("cm|mm|pt", RegexOptions.Compiled);

            MatchCollection matched = regx.Matches(reportBody);

            label2.Text = string.Format("Znalazłem pozycji do modyfikacji {0}", matched.Count);

            foreach (var item in matched)
            {
                
                var matchedValue = item.ToString();
                string rounded = "";
                if (regxMeasure.IsMatch(matchedValue))
                {
                    Match m = regxMeasure.Match(matchedValue);
                    matchedValue = matchedValue.Replace(m.Value, "");

                    switch (m.Value)
                    {

                        case "mm":
                            rounded = ConvertMilimetersToPoint(decimal.Parse(matchedValue, System.Globalization.NumberFormatInfo.InvariantInfo)).ToString();
                            break;
                        case "cm":
                            rounded = ConvertCentimetersToPoint(decimal.Parse(matchedValue, System.Globalization.NumberFormatInfo.InvariantInfo)).ToString();
                            break;
                        case "pt":
                            rounded = ConvertPointsDecimalToIntToPoint(decimal.Parse(matchedValue, System.Globalization.NumberFormatInfo.InvariantInfo)).ToString();
                            break;
                    }
                    matchedValue = string.Format("{0}{1}", matchedValue, m.Value);

                    richTextBox1.AppendText(
                        string.Format("Zmodyfikowałem wymiar {0} jednostki {1} teraz jest: {2}{3}{4}", 
                        item, 
                        m.Value, 
                        rounded,
                        "pt",
                        Environment.NewLine));
                }
                else
                {
                    IgnoredMeasure(item);
                }

                reportBody = reportBody.Replace(matchedValue, string.Format("{0}pt",rounded));
            }

            return reportBody;

        }

        private void IgnoredMeasure(object item)
        {
            richTextBox1.SelectionColor = Color.Red;
            richTextBox1.AppendText(string.Format("zignorowałem wymiar {0} {1}", item,Environment.NewLine));
            richTextBox1.SelectionColor = Color.Black;
        }

        private string ConvertMilimetersToPoint(decimal milimeters)
        {
            

            return GetDecimalRound(milimeters * Onemilimeter).ToString();

        }

        private string ConvertCentimetersToPoint(decimal centimeters)
        {
           

            return GetDecimalRound(centimeters * OneCentimeter).ToString();

        }

        private string ConvertPointsDecimalToIntToPoint(decimal points)
        {
            return GetDecimalRound(points).ToString();
        }

        private void openToolStripMenuItem_Click(object ender, EventArgs e)
        {
            
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                
                System.IO.StreamReader sr = new
                   System.IO.StreamReader(openFileDialog1.FileName);
                rdlcontent = sr.ReadToEnd();

                sr.Close();


                rdlcontent = FindPositionsToConvertAndFixThem(rdlcontent);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            saveFileDialog1.ShowDialog();
        }

        private async void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string name = saveFileDialog1.FileName;
            using (StreamWriter outputFile = new StreamWriter(name))
            {
                await outputFile.WriteAsync(rdlcontent);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
